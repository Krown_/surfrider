# Installation :

Copie de l'environnement:

`cp .env.example .env`

Génération de l'application key

`php artisan key:generate`

Installation des dépendances PHP:

`composer install`

Installation des dépendances NodeJS:

`npm install`

---

__Défi Among Us:__
Tous les personnages sont visibles lors d'un sabotage (404)

L'imposteur est de couleur **cyan**

Il est trouvable sur la page d'accueil, caché dans les vagues.
Vous pouvez tous les zigouillés, cela fera apparaître une alerte vous indiquant si vous avez eu le bon.

__Défi Toucan Toco:__
On a essayé, on a des requêtes qui fonctionnent.
Impossible de mettre la balise `<iframe>` sur nos pages.

__Défi Sogeti:__
On a cherché des données, on en a pas trouvé

__Défi Nuit de l'Info 2020:__
L'utilisateur/surfeur peut s'inscrire et se connecter.
Il a à sa disposition une map avec plusieurs spots de surf, sur lesquels il peut ajouter des incidents.
Chaque spot possède un lien vers une livecam, des données météos ainsi qu'un report des derniers incidents.
