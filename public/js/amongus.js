/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/amongus.js":
/*!*********************************!*\
  !*** ./resources/js/amongus.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$(\".amoungus\").on('click', function (e) {\n  var strImpostor = \" was the Impostor\";\n  var strNotImpostor = \" was not the Impostor\";\n  alert($(this).data('color') + ($(this).data('color') === 'cyan' ? strImpostor : strNotImpostor));\n  $(this).remove();\n});\n/*const { upperFirst } = require(\"lodash\");\r\n\r\nconsole.debug('Module imposteur chargé');\r\n\r\nlet app = document.getElementById(\"app\");\r\nlet container = document.getElementById(\"container-amongus\");\r\n\r\nlet imageUrl = app.getAttribute(\"data-imageurl\") + \"/img/\"\r\n\r\nlet colors = [\r\n    'black',\r\n    'blue',\r\n    'brown',\r\n    'cyan',\r\n    'fortegreen',\r\n    'green',\r\n    'lightblue',\r\n    'lime',\r\n    'orange',\r\n    'pink',\r\n    'purple',\r\n    'red',\r\n    'tan',\r\n    'white',\r\n    'yellow'\r\n]\r\n\r\n\r\nlet impostorColor = colors[11]\r\nconsole.debug(\"Impostor color : \" + impostorColor)\r\n\r\nlet charList = document.getElementsByClassName('amongus-char');\r\n// console.debug(chars)\r\n\r\n//On fait pop plusieurs personnages\r\nif (charList.length == 0) {\r\n    console.debug(\"Aucun personnage trouvé\")\r\n    let nbMax = 1 + Math.floor(Math.random()*3)\r\n    console.debug(nbMax)\r\n    for(let i = 0; i<nbMax; i++) {\r\n        let newCharacter = document.createElement(\"img\");\r\n        let colorID = Math.floor(Math.random()*colors.length)\r\n        let randomColor = colors[colorID];\r\n\r\n        colors = colors.filter(color => {\r\n            // console.log(\"DEBUG:\" + color)\r\n            return color !== randomColor\r\n        })\r\n        console.log(colors)\r\n\r\n        let randomX = Math.random()*window.outerWidth;\r\n        let randomY = Math.random()*window.outerHeight;\r\n\r\n        // let randomX = 0;\r\n        // let randomY = 0;\r\n\r\n        console.debug('X:'+randomX, 'Y:'+randomY)\r\n\r\n        let filename = imageUrl+randomColor+\".png\"\r\n\r\n        newCharacter.setAttribute('class', 'amongus-char')\r\n        newCharacter.setAttribute('src', filename)\r\n        newCharacter.setAttribute('id', randomColor)\r\n        newCharacter.setAttribute('style', \"width: 56px; z-index: 42; position: absolute; transform: translate(\"+randomX+\"px,\"+randomY+\"px);\")\r\n\r\n        container.append(newCharacter)\r\n\r\n\r\n        console.debug(i+1 + \" Personnage crée\")\r\n        console.debug(newCharacter)\r\n    }\r\n}\r\n\r\n//Ré-assignation\r\n// charList = document.getElementsByClassName('amongus-char');\r\n\r\n\r\n//Assigne l'event pour chaque .amongus-char\r\nfor(let char of charList) {\r\n    char.addEventListener(\"click\", (e) => {\r\n        let id = char.getAttribute('id');\r\n        let strImpostor = \" was the Impostor\"\r\n        let strNotImpostor = \" was not the Impostor\"\r\n\r\n        alert(upperFirst(id) + (id === impostorColor ? strImpostor : strNotImpostor))\r\n        document.getElementById(id).remove()\r\n    });\r\n    // console.debug(typeof(char))\r\n    // console.debug(char.getAttribute('id'));\r\n}\r\n*///# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvYW1vbmd1cy5qcz83OGQ3Il0sIm5hbWVzIjpbIiQiLCJvbiIsImUiLCJzdHJJbXBvc3RvciIsInN0ck5vdEltcG9zdG9yIiwiYWxlcnQiLCJkYXRhIiwicmVtb3ZlIl0sIm1hcHBpbmdzIjoiQUFBQUEsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlQyxFQUFmLENBQWtCLE9BQWxCLEVBQTJCLFVBQVNDLENBQVQsRUFBVztBQUNsQyxNQUFJQyxXQUFXLEdBQUcsbUJBQWxCO0FBQ0EsTUFBSUMsY0FBYyxHQUFHLHVCQUFyQjtBQUNBQyxPQUFLLENBQUNMLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUU0sSUFBUixDQUFhLE9BQWIsS0FBeUJOLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUU0sSUFBUixDQUFhLE9BQWIsTUFBMEIsTUFBMUIsR0FBbUNILFdBQW5DLEdBQWlEQyxjQUExRSxDQUFELENBQUw7QUFDQUosR0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRTyxNQUFSO0FBQ0gsQ0FMRDtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Ii4vcmVzb3VyY2VzL2pzL2Ftb25ndXMuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIkKFwiLmFtb3VuZ3VzXCIpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpe1xyXG4gICAgbGV0IHN0ckltcG9zdG9yID0gXCIgd2FzIHRoZSBJbXBvc3RvclwiXHJcbiAgICBsZXQgc3RyTm90SW1wb3N0b3IgPSBcIiB3YXMgbm90IHRoZSBJbXBvc3RvclwiXHJcbiAgICBhbGVydCgkKHRoaXMpLmRhdGEoJ2NvbG9yJykgKyAoJCh0aGlzKS5kYXRhKCdjb2xvcicpID09PSAnY3lhbicgPyBzdHJJbXBvc3RvciA6IHN0ck5vdEltcG9zdG9yKSlcclxuICAgICQodGhpcykucmVtb3ZlKCk7XHJcbn0pO1xyXG5cclxuLypjb25zdCB7IHVwcGVyRmlyc3QgfSA9IHJlcXVpcmUoXCJsb2Rhc2hcIik7XHJcblxyXG5jb25zb2xlLmRlYnVnKCdNb2R1bGUgaW1wb3N0ZXVyIGNoYXJnw6knKTtcclxuXHJcbmxldCBhcHAgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFwcFwiKTtcclxubGV0IGNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY29udGFpbmVyLWFtb25ndXNcIik7XHJcblxyXG5sZXQgaW1hZ2VVcmwgPSBhcHAuZ2V0QXR0cmlidXRlKFwiZGF0YS1pbWFnZXVybFwiKSArIFwiL2ltZy9cIlxyXG5cclxubGV0IGNvbG9ycyA9IFtcclxuICAgICdibGFjaycsXHJcbiAgICAnYmx1ZScsXHJcbiAgICAnYnJvd24nLFxyXG4gICAgJ2N5YW4nLFxyXG4gICAgJ2ZvcnRlZ3JlZW4nLFxyXG4gICAgJ2dyZWVuJyxcclxuICAgICdsaWdodGJsdWUnLFxyXG4gICAgJ2xpbWUnLFxyXG4gICAgJ29yYW5nZScsXHJcbiAgICAncGluaycsXHJcbiAgICAncHVycGxlJyxcclxuICAgICdyZWQnLFxyXG4gICAgJ3RhbicsXHJcbiAgICAnd2hpdGUnLFxyXG4gICAgJ3llbGxvdydcclxuXVxyXG5cclxuXHJcbmxldCBpbXBvc3RvckNvbG9yID0gY29sb3JzWzExXVxyXG5jb25zb2xlLmRlYnVnKFwiSW1wb3N0b3IgY29sb3IgOiBcIiArIGltcG9zdG9yQ29sb3IpXHJcblxyXG5sZXQgY2hhckxpc3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdhbW9uZ3VzLWNoYXInKTtcclxuLy8gY29uc29sZS5kZWJ1ZyhjaGFycylcclxuXHJcbi8vT24gZmFpdCBwb3AgcGx1c2lldXJzIHBlcnNvbm5hZ2VzXHJcbmlmIChjaGFyTGlzdC5sZW5ndGggPT0gMCkge1xyXG4gICAgY29uc29sZS5kZWJ1ZyhcIkF1Y3VuIHBlcnNvbm5hZ2UgdHJvdXbDqVwiKVxyXG4gICAgbGV0IG5iTWF4ID0gMSArIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSozKVxyXG4gICAgY29uc29sZS5kZWJ1ZyhuYk1heClcclxuICAgIGZvcihsZXQgaSA9IDA7IGk8bmJNYXg7IGkrKykge1xyXG4gICAgICAgIGxldCBuZXdDaGFyYWN0ZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW1nXCIpO1xyXG4gICAgICAgIGxldCBjb2xvcklEID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpKmNvbG9ycy5sZW5ndGgpXHJcbiAgICAgICAgbGV0IHJhbmRvbUNvbG9yID0gY29sb3JzW2NvbG9ySURdO1xyXG5cclxuICAgICAgICBjb2xvcnMgPSBjb2xvcnMuZmlsdGVyKGNvbG9yID0+IHtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJERUJVRzpcIiArIGNvbG9yKVxyXG4gICAgICAgICAgICByZXR1cm4gY29sb3IgIT09IHJhbmRvbUNvbG9yXHJcbiAgICAgICAgfSlcclxuICAgICAgICBjb25zb2xlLmxvZyhjb2xvcnMpXHJcblxyXG4gICAgICAgIGxldCByYW5kb21YID0gTWF0aC5yYW5kb20oKSp3aW5kb3cub3V0ZXJXaWR0aDtcclxuICAgICAgICBsZXQgcmFuZG9tWSA9IE1hdGgucmFuZG9tKCkqd2luZG93Lm91dGVySGVpZ2h0O1xyXG5cclxuICAgICAgICAvLyBsZXQgcmFuZG9tWCA9IDA7XHJcbiAgICAgICAgLy8gbGV0IHJhbmRvbVkgPSAwO1xyXG5cclxuICAgICAgICBjb25zb2xlLmRlYnVnKCdYOicrcmFuZG9tWCwgJ1k6JytyYW5kb21ZKVxyXG5cclxuICAgICAgICBsZXQgZmlsZW5hbWUgPSBpbWFnZVVybCtyYW5kb21Db2xvcitcIi5wbmdcIlxyXG5cclxuICAgICAgICBuZXdDaGFyYWN0ZXIuc2V0QXR0cmlidXRlKCdjbGFzcycsICdhbW9uZ3VzLWNoYXInKVxyXG4gICAgICAgIG5ld0NoYXJhY3Rlci5zZXRBdHRyaWJ1dGUoJ3NyYycsIGZpbGVuYW1lKVxyXG4gICAgICAgIG5ld0NoYXJhY3Rlci5zZXRBdHRyaWJ1dGUoJ2lkJywgcmFuZG9tQ29sb3IpXHJcbiAgICAgICAgbmV3Q2hhcmFjdGVyLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBcIndpZHRoOiA1NnB4OyB6LWluZGV4OiA0MjsgcG9zaXRpb246IGFic29sdXRlOyB0cmFuc2Zvcm06IHRyYW5zbGF0ZShcIityYW5kb21YK1wicHgsXCIrcmFuZG9tWStcInB4KTtcIilcclxuXHJcbiAgICAgICAgY29udGFpbmVyLmFwcGVuZChuZXdDaGFyYWN0ZXIpXHJcblxyXG5cclxuICAgICAgICBjb25zb2xlLmRlYnVnKGkrMSArIFwiIFBlcnNvbm5hZ2UgY3LDqWVcIilcclxuICAgICAgICBjb25zb2xlLmRlYnVnKG5ld0NoYXJhY3RlcilcclxuICAgIH1cclxufVxyXG5cclxuLy9Sw6ktYXNzaWduYXRpb25cclxuLy8gY2hhckxpc3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdhbW9uZ3VzLWNoYXInKTtcclxuXHJcblxyXG4vL0Fzc2lnbmUgbCdldmVudCBwb3VyIGNoYXF1ZSAuYW1vbmd1cy1jaGFyXHJcbmZvcihsZXQgY2hhciBvZiBjaGFyTGlzdCkge1xyXG4gICAgY2hhci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKGUpID0+IHtcclxuICAgICAgICBsZXQgaWQgPSBjaGFyLmdldEF0dHJpYnV0ZSgnaWQnKTtcclxuICAgICAgICBsZXQgc3RySW1wb3N0b3IgPSBcIiB3YXMgdGhlIEltcG9zdG9yXCJcclxuICAgICAgICBsZXQgc3RyTm90SW1wb3N0b3IgPSBcIiB3YXMgbm90IHRoZSBJbXBvc3RvclwiXHJcblxyXG4gICAgICAgIGFsZXJ0KHVwcGVyRmlyc3QoaWQpICsgKGlkID09PSBpbXBvc3RvckNvbG9yID8gc3RySW1wb3N0b3IgOiBzdHJOb3RJbXBvc3RvcikpXHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWQpLnJlbW92ZSgpXHJcbiAgICB9KTtcclxuICAgIC8vIGNvbnNvbGUuZGVidWcodHlwZW9mKGNoYXIpKVxyXG4gICAgLy8gY29uc29sZS5kZWJ1ZyhjaGFyLmdldEF0dHJpYnV0ZSgnaWQnKSk7XHJcbn1cclxuKi9cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/amongus.js\n");

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./resources/js/amongus.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\wamp\www\surfrider\resources\js\amongus.js */"./resources/js/amongus.js");


/***/ })

/******/ });