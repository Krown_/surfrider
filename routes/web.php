<?php

use App\Http\Controllers\IncidentController;
use App\Models\Produit;
use App\Models\Spot;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/reports', [IncidentController::class, 'index'])
    ->name('reports');

Route::get('/spots', function() {
    return Spot::all()->toJson();
});

Route::get('/report/{spotId}', [IncidentController::class, 'create'])
    ->name('report.create');

Route::post('/report/{spotId}', [IncidentController::class, 'store'])
    ->name('report.store');

Route::get('/produits', function () {
        return Produit::all()->toJson();
})->name('produits');


Route::get('/maps', function(){return view('maps');})->name('maps');
Route::get('/spot/{id}', [App\Http\Controllers\SpotController::class, 'index'])->name('spot');

Auth::routes();
