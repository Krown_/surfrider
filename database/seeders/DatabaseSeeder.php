<?php

namespace Database\Seeders;

use App\Models\Produit;
use App\Models\Spot;
use App\Models\Ville;
use App\Models\VillesSpot;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $villes = array(
            array(
                'id'        => 1,
                'nom'   => 'Fort-Mahon-Plage',
                'latitude'   => '50.341429',
                'longitude'   => '1.568313',
            ),
            array(
                'id'        => 2,
                'nom'   => 'Le Crotoy',
                'latitude'   => '50.216569',
                'longitude'   => '1.624047',
            ),
            array(
                'id'        => 3,
                'nom'   => 'Trouville-sur-Mer',
                'latitude'   => '49.368333',
                'longitude'   => '0.0825',
            ),
            array(
                'id'        => 4,
                'nom'   => 'Saint-Pair-Sur-Mer',
                'latitude'   => '48.813913',
                'longitude'   => '-1.566595',
            ),
            array(
                'id'        => 5,
                'nom'   => 'Saint-Malo',
                'latitude'   => '48.645453',
                'longitude'   => '-2.015418',
            ),
            array(
                'id'        => 6,
                'nom'   => 'Plougasnou',
                'latitude'   => '48.695913',
                'longitude'   => '-3.79223',
            ),
            array(
                'id'        => 7,
                'nom'   => 'Plougonvelin',
                'latitude'   => '48.3415116',
                'longitude'   => '-4.7192399',
            ),
            array(
                'id'        => 8,
                'nom'   => 'Plogoff',
                'latitude'   => '48.037737',
                'longitude'   => '-4.666262',
            ),
            array(
                'id'        => 9,
                'nom'   => 'Barbâtre',
                'latitude'   => '46.944722',
                'longitude'   => '-2.179444',
            ),
            array(
                'id'        => 10,
                'nom'   => 'Île d\'Orléron',
                'latitude'   => '45.923547',
                'longitude'   => '-1.287811',
            ),
            array(
                'id'        => 11,
                'nom'   => 'Biarritz',
                'latitude'   => '43.471144',
                'longitude'   => '-1.552727',
            ),
            array(
                'id'        => 12,
                'nom'   => 'Hyères',
                'latitude'   => '43.120257',
                'longitude'   => '6.130161',
            ),
            array(
                'id'        => 13,
                'nom'   => 'Saint-Laurent-du-Var',
                'latitude'   => '43.66901',
                'longitude'   => '7.190697',
            ),
        );

        Ville::insert($villes);

        $spots = array(
            array(
                'id'        => 1,
                'nom'   => 'Fort-Mahon-Plage',
                'latitude'   => '50.35',
                'longitude'   => '1.5667',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/axis/fortmahon/fortmahon.mjpg',
            ),
            array(
                'id'        => 2,
                'nom'   => 'Port de Plaisance',
                'latitude'   => '50.216952',
                'longitude'   => '1.630922',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/lecrotoy0.php',
            ),
            array(
                'id'        => 3,
                'nom'   => 'Plage',
                'latitude'   => '50.214597',
                'longitude'   => '1.621937',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/lecrotoy20.php',
            ),
            array(
                'id'        => 4,
                'nom'   => 'Trouville-sur-Mer',
                'latitude'   => '49.368632',
                'longitude'   => '0.079305',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/trouville0.php',
            ),
            array(
                'id'        => 5,
                'nom'   => 'Saint-Pair-Sur-Mer',
                'latitude'   => '48.814788',
                'longitude'   => '-1.572689',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/stpair0.php',
            ),
            array(
                'id'        => 6,
                'nom'   => 'Hôtel Beaufort',
                'latitude'   => '48.655007',
                'longitude'   => '-2.006953',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/beaufort0.php',
            ),
            array(
                'id'        => 7,
                'nom'   => 'Les Thermes Marins',
                'latitude'   => '48.658214',
                'longitude'   => '-1.999394',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/stmalo40.php',
            ),
            array(
                'id'        => 8,
                'nom'   => 'Les Forts',
                'latitude'   => '48.651301',
                'longitude'   => '-2.023077',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/stmalo0.php',
            ),
            array(
                'id'        => 9,
                'nom'   => 'Térénez',
                'latitude'   => '48.676295',
                'longitude'   => '-3.851413',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/plougasnou0.php',
            ),
            array(
                'id'        => 10,
                'nom'   => 'Le Trez Hir',
                'latitude'   => '48.347211',
                'longitude'   => '-4.703481',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/plougonvelin0.php',
            ),
            array(
                'id'        => 11,
                'nom'   => 'La Pointe Du Raz',
                'latitude'   => '48.039983',
                'longitude'   => '-4.741124',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/raz0.php',
            ),
            array(
                'id'        => 12,
                'nom'   => 'Barbâtre ',
                'latitude'   => '46.937088',
                'longitude'   => '-2.181319',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/barbatre0.php',
            ),
            array(
                'id'        => 13,
                'nom'   => 'La Cotinière',
                'latitude'   => '45.913466',
                'longitude'   => '-1.328632',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/cotiniere0.php',
            ),
            array(
                'id'        => 14,
                'nom'   => 'Biarritz',
                'latitude'   => '43.483289',
                'longitude'   => '-1.568863',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/biarritz0.php',
            ),
            array(
                'id'        => 15,
                'nom'   => 'La Madrague',
                'latitude'   => '43.03919',
                'longitude'   => '6.112695',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/madrague0.php',
            ),
            array(
                'id'        => 16,
                'nom'   => 'Porquerolles',
                'latitude'   => '43.034791',
                'longitude'   => '6.169243',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/hyeres20.php',
            ),
            array(
                'id'        => 17,
                'nom'   => 'Saint-Laurent-du-Var',
                'latitude'   => '43.657038',
                'longitude'   => '7.184104',
                'urlWebcam'   => 'https://www.vision-environnement.com/live/player/saintlaurentduvar0.php',
            ),
        );

        Spot::insert($spots);

        $villesSpots = array(
            array(
                'ville_id'  => 1,
                'spot_id'   => 1,
            ),
            array(
                'ville_id'  => 2,
                'spot_id'   => 2,
            ),
            array(
                'ville_id'  => 2,
                'spot_id'   => 3,
            ),
            array(
                'ville_id'  => 3,
                'spot_id'   => 4,
            ),
            array(
                'ville_id'  => 4,
                'spot_id'   => 5,
            ),
            array(
                'ville_id'  => 5,
                'spot_id'   => 6,
            ),
            array(
                'ville_id'  => 5,
                'spot_id'   => 7,
            ),
            array(
                'ville_id'  => 5,
                'spot_id'   => 8,
            ),
            array(
                'ville_id'  => 6,
                'spot_id'   => 9,
            ),
            array(
                'ville_id'  => 7,
                'spot_id'   => 10,
            ),
            array(
                'ville_id'  => 8,
                'spot_id'   => 11,
            ),
            array(
                'ville_id'  => 9,
                'spot_id'   => 12,
            ),
            array(
                'ville_id'  => 10,
                'spot_id'   => 13,
            ),
            array(
                'ville_id'  => 11,
                'spot_id'   => 14,
            ),
            array(
                'ville_id'  => 12,
                'spot_id'   => 15,
            ),
            array(
                'ville_id'  => 12,
                'spot_id'   => 16,
            ),
            array(
                'ville_id'  => 13,
                'spot_id'   => 17,
            ),
        );

        VillesSpot::insert($villesSpots);

        $produits = array(
            array(
                "nom"   => "Crème solaire",
            ),
            array(
                "nom"   => "Parfum / Déodorant",
            ),
            array(
                "nom"   => "Crème hydratante",
            ),
            array(
                "nom"   => "Maquillage",
            ),
            array(
                "nom"   => "Essence",
            ),
            array(
                "nom"   => "Cigarette",
            ),
            array(
                "nom"   => "Engrais / pesticides",
            ),
            array(
                "nom"   => "Peintures",
            ),
            array(
                "nom"   => "Autres",
            ),
        );
        Produit::insert($produits);
    }
}
