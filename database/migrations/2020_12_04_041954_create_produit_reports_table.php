<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produit_reports', function (Blueprint $table) {
            $table->unsignedBigInteger('produit_id')->nullable();
            $table->foreign('produit_id')
                ->references('id')->on('produits')
                ->onDelete('cascade');

            $table->unsignedBigInteger('report_id')->nullable();
            $table->foreign('report_id')
                ->references('id')->on('incidents')
                ->onDelete('cascade');

            $table->primary(['produit_id', 'report_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produit_reports');
    }
}
