<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotsActivitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spots_activites', function (Blueprint $table) {
            $table->unsignedBigInteger('spot_id')->nullable();
            $table->foreign('spot_id')
                ->references('id')->on('spots')
                ->onDelete('cascade');

            $table->unsignedBigInteger('activite_id')->nullable();
            $table->foreign('activite_id')
                ->references('id')->on('activites')
                ->onDelete('cascade');

            $table->primary(['spot_id', 'activite_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spots_activites');
    }
}
