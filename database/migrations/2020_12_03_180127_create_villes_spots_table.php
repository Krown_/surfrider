<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVillesSpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('villes_spots', function (Blueprint $table) {
            $table->unsignedBigInteger('ville_id')->nullable();
            $table->foreign('ville_id')
                ->references('id')->on('villes')
                ->onDelete('cascade');

            $table->unsignedBigInteger('spot_id')->nullable();
            $table->foreign('spot_id')
                ->references('id')->on('spots')
                ->onDelete('cascade');

            $table->primary(['ville_id', 'spot_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('villes_spots');
    }
}
