<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotsIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spots_incidents', function (Blueprint $table) {
            $table->unsignedBigInteger('spot_id')->nullable();
            $table->foreign('spot_id')
                ->references('id')->on('spots')
                ->onDelete('cascade');

            $table->unsignedBigInteger('incident_id')->nullable();
            $table->foreign('incident_id')
                ->references('id')->on('incidents')
                ->onDelete('cascade');

            $table->primary(['spot_id', 'incident_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spots_incidents');
    }
}
