const { ajax } = require("jquery")

console.log("SCRIPTS PRODUITS OK")

let select = document.getElementById('select-produits');
let dataUrl = select.getAttribute("data-uri");

console.log(dataUrl);

let xhttp = new XMLHttpRequest()
xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
        console.log("XHTTP DEBUG:");
        let data = JSON.parse(this.responseText);
        console.log(data);

        Object.values(data).forEach((value) => {
            let option = document.createElement("option")
            option.setAttribute("value", value.id)
            option.innerHTML = value.nom
            select.appendChild(option)
            // console.log(option)
        })

        // for(let [key, value] in Object.entries(data)) {
        //     console.log(value)
        //     let option = document.createElement("option")
        //     option.setAttribute("value", value.id)
        //     option.innerHTML = value.nom
        //     select.appendChild(option)
        // }
    }
}

xhttp.open('GET', dataUrl, true);
xhttp.send();
