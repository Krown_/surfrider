@extends('layouts.app')

@section('title', 'Accueil')

@push('styles')
    <link href="{{ asset('css/accueil.css') }}" rel="stylesheet">
@endpush


@section('content')

    <div class="banner">
        <img src="{{asset('img/accueil.jpg')}}">
        <div class="bannerText center">
            <h1>SurfRider</h1>
            <div class="amoungus cyan" data-color="cyan">
                <img src="{{asset('img/cyan.png')}}">
            </div>
            <p>Toutes les informations sur tes spots préférés</p>
        </div>
    </div>

    <div class="separator center">
        <h1>Carte des spots de France</h1>
    </div>

    <div class="center">
        <div id="map">
        </div>
        <div class="amoungus purple" data-color="purple">
            <img src="{{asset('img/purple.png')}}">
        </div>
        <div class="amoungus brown" data-color="brown">
            <img src="{{asset('img/brown.png')}}">
        </div>
    </div>

    <div class="stats">
        <div class="infos">
            <h1>Nombre de spots</h1>
            <p>{{$nbSpots}}</p>
        </div>
        <div class="infos">
            <h1>Nombre d'incidents</h1>
            <p>{{$nbIncidents}}</p>
        </div>
    </div>

    <div class="statsGouv">
        <!-- Stats gouvernement -->
    </div>


    <script>

        let map;

        var lati = 46.66533281844335 ;
        var longi = 2.8911389083123673 ;

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                center: { lat: lati, lng: longi},
                zoom: 6,
            });

            @foreach($spots as $spot)
            const marker{{$spot->id}} = new google.maps.Marker({
            position : new google.maps.LatLng({{$spot->latitude}},{{$spot->longitude}}),
            icon: "{{asset('img/pointeur.png')}}",
            map: map,
            });

            const contentString{{$spot->id}} = '<div><h5>{{$spot->nom}}</h5></div>'+
            "<div><p>Nombre d'incident : {{count($spot->incidents)}}</p></div>"+
            '<div><a class="btn btn-primary" href="/spot/{{$spot->id}}">Infos Spot</a></div>'

            const infoBulle{{$spot->id}} = new google.maps.InfoWindow({

                content : contentString{{$spot->id}},

            });


            marker{{$spot->id}}.addListener("click", () => {
                infoBulle{{$spot->id}}.open(map, marker{{$spot->id}});
            });

            @endforeach
        }





    </script>




@endsection
