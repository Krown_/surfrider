@extends('layouts.app')

@section('title', 'Spot')

@push('styles')
    <link href="{{ asset('css/spot.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container center">
        <h1>{{$spot->nom}}</h1>
        <div class="spotCams">
            <iframe src={{$spot->urlWebcam}} allowfullscreen scrolling="no" allow="autoplay; encrypted-media"></iframe>
        </div>
    </div>

    <div class="center" style="padding-top: 25px">
        <a href="/report/{{$spot->id}}" class="btn btn-danger">Déclarer un incident</a>
    </div>

    <div class="center" style="padding-top: 25px" id="openweathermap-widget-21"></div>

    <div class="spotInformation">
        <div>
            <h2>Dernier incidents</h2>
            <div class="center">


                @if(count($spot->incidents)>0)

                @for($i=0;$i<=(count($spot->incidents)<=3?count($spot->incidents)-1:2);++$i)

                    <p>{{$spot->incidents[$i]->incident->type}} le {{$spot->incidents[$i]->incident->date}} à {{$spot->incidents[$i]->incident->heureDeb}}</p>

                @endfor

                @else

                    <p>Aucun incident !</p>

                @endif
            </div>
        </div>
        <div>
            <h2>Informations Spot</h2>
            <div class="center">
                <p>Ville : {{$spot->villes[0]->ville->name}}</p>
                <p>Longitude : {{$spot->longitude}}</p>
                <p>Latitude : {{$spot->latitude}}</p>
            </div>
        </div>
    </div>
    <script>

        var lati = {{$spot->villes[0]->ville->latitude}} ;
      var longi = {{$spot->villes[0]->ville->longitude}} ;

    fetch('https://api.openweathermap.org/data/2.5/weather?lat='+lati+'&lon='+longi+'&units=metric&appid=9cb50f4ec570a22bda42b695af20bed6')
        .then(response => response.json())
        .then(data => {
            window.myWidgetParam ? window.myWidgetParam : window.myWidgetParam = [];  window.myWidgetParam.push({id: 21,cityid: data['id'],appid: '9cb50f4ec570a22bda42b695af20bed6',units: 'metric',containerid: 'openweathermap-widget-21',  });  (function() {var script = document.createElement('script');script.async = true;script.charset = "utf-8";script.src = "//openweathermap.org/themes/openweathermap/assets/vendor/owm/js/weather-widget-generator.js";var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(script, s);  })();
        })
        //.then(data => console.log(data))
        .catch(err => console.log('Erreur coordonnées'))
        </script>

@endsection
