@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 p-5 text-center">
            <img src={{asset('img/sabotage.png')}} alt="" style="width: auto;">
            <h1>La page demandée est introuvable...</h1>
        </div>
        <div class="col-md-12 text-center p-5">
            <img src={{asset('img/black.png')}} style="width: 112px;" class="amongus-char" id="black">
            <img src={{asset('img/blue.png')}} style="width: 112px;" class="amongus-char" id="blue">
            <img src={{asset('img/brown.png')}} style="width: 112px;" class="amongus-char" id="brown">
            <img src={{asset('img/cyan.png')}} style="width: 112px;" class="amongus-char" id="cyan">
            <img src={{asset('img/fortegreen.png')}} style="width: 112px;" class="amongus-char" id="fortegreen">
            <img src={{asset('img/green.png')}} style="width: 112px;" class="amongus-char" id="green">
            <img src={{asset('img/lightblue.png')}} style="width: 112px;" class="amongus-char" id="lightblue">
            <img src={{asset('img/lime.png')}} style="width: 112px;" class="amongus-char" id="lime">
            <img src={{asset('img/orange.png')}} style="width: 112px;" class="amongus-char" id="orange">
            <img src={{asset('img/pink.png')}} style="width: 112px;" class="amongus-char" id="pink">
            <img src={{asset('img/purple.png')}} style="width: 112px;" class="amongus-char" id="purple">
            <img src={{asset('img/red.png')}} style="width: 112px;" class="amongus-char" id="red">
            <img src={{asset('img/tan.png')}} style="width: 112px;" class="amongus-char" id="tan">
            <img src={{asset('img/white.png')}} style="width: 112px;" class="amongus-char" id="white">
            <img src={{asset('img/yellow.png')}} style="width: 112px;" class="amongus-char" id="yellow">
        </div>
    </div>
</div>
</div>
<script src={{asset('js/amongus.js')}} defer></script>
@endsection
