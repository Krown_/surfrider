@extends('layouts.app')

@section('content')
<style>
    /* Style pour les champs requis */
    span.required::after{
        content: "*";
        color: red;
    }

</style>
<section class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 text-center">
            <h1>
                Reporter un nouvel incident pour {{$spot->nom}}
            </h1>
        </div>
    </div>
</section>
<section class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action={{route('report.store', $spot->id)}} method="POST">
                @csrf

                <div class="form-group">
                    <label for="type">Type de l'incident</label><span class="required"></span>
                <input type="text" name="type" class="form-control" placeholder="Cuvette de toilette" required>
                </div>
                <div class="form-group">
                    <label for="date">Date de l'incident</label><span class="required"></span>
                    <input type="date" name="date" class="form-control"  value="{{Carbon\Carbon::parse(Carbon\Carbon::now())->format('d/m/Y')}}" required>
                </div>

                <div class="form-group">
                    <label for="heureDeb">Heure de début (approximative)</label><span class="required"></span>
                    <input type="time" name="heureDeb" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="heureFin">Heure de fin (approximative)</label><span class="required"></span>
                    <input type="time" name="heureFin" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="produits">Produits utilisés lors de votre session</label>
                    <select name="produits" class="form-control" data-uri={{route('produits')}} id="select-produits">
                        <option value=0 selected>Aucun</option>
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-outline-primary btn-block">Soumettre l'incident</button>
                </div>
            </form>
        </div>
    </div>
</section>

@push('scripts')
    <script src={{asset('js/produits.js')}}></script>
@endpush

@endsection
