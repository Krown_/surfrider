<?php

namespace App\Http\Controllers;

use App\Models\Incident;
use App\Models\Spot;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nbSpots = count(Spot::all());
        $spot = Spot::all();
        $nbIncidents = count(Incident::all());

        return view('home', ["nbSpots" => $nbSpots, "nbIncidents" => $nbIncidents, 'spots' => $spot]);
    }
}
