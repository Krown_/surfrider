<?php

namespace App\Http\Controllers;
use App\Models\Spot;


use Illuminate\Http\Request;

class SpotController extends Controller
{
    public function index(int $id){

        $spot = Spot::with('incidents.incident','villes.ville')->findOrFail($id);

        return view('spot', compact('spot'));

    }
}
