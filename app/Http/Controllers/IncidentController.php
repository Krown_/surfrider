<?php

namespace App\Http\Controllers;

use App\Models\Incident;
use App\Models\Spot;
use App\Models\SpotsIncident;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IncidentController extends Controller
{
    public function index() {
        return Incident::all()->toJson();
    }

    public function create(int $spotId) {
        $spot = Spot::findOrFail($spotId);


        if(($user = Auth::user()) === null) {
            return redirect()->to('login')->with('error', 'Vous devez être authentifié pour signaler un incident');
        }
        return view('forms.create-incident', compact('spot'));
    }

    public function store(Request $request, int $spotId) {
        if(($user = Auth::user()) === null) {
            return redirect()->to('login')->with('error', 'Vous devez être authentifié pour signaler un incident');
        }

        $data = $request->validate([
            'type'      => 'required',
            'date'      => 'required',
            'heureDeb'  => 'required',
            'heureFin'  => 'required',
        ]);

        $data['user_id'] = $user->id;

        if($request->has('produits'))


        $incident = new Incident($data);
        $incident->save();

        $dataSpotIncident = [
            'spot_id'       => $spotId,
            'incident_id'   => $incident->id,
        ];

        $spotIncident = new SpotsIncident($dataSpotIncident);
        $spotIncident->save();

        return redirect()->route('home')->with('success', "Merci d'avoir signaler cet incident, il a bien été enregistré");
    }

    public function update() {
        //
    }

    public function delete() {
        //
    }
}
