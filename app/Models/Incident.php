<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    use HasFactory;

    public $fillable = [
        'type',
        'heureDeb',
        'heureFin',
        'date',
        'user_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function spots()
    {
        return $this->hasOne('App\Models\SpotsIncident');
    }
}
