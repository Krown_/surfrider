<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spot extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'nom',
        'latitude',
        'longitude',
        'urlWebcam',
    ];

    public function activites()
    {
        return $this->hasManyThrough('App\Models\Activite', 'App\Models\SpotsActivite');
    }

    public function incidents()
    {
        return $this->hasMany('App\Models\SpotsIncident');
    }

    public function villes()
    {
        return $this->hasMany('App\Models\VillesSpot');
    }

}
