<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpotsActivite extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function activites()
    {
        return $this->belongsTo('App\Models\Activite');
    }

    public function spots()
    {
        return $this->belongsTo('App\Models\Spot');
    }
}
