<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpotsIncident extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = [
        'spot_id',
        'incident_id',
    ];

    public function incident()
    {
        return $this->belongsTo('App\Models\Incident');
    }

    public function spots()
    {
        return $this->hasOne('App\Models\Spot');
    }
}
