<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ville extends Model
{
    use HasFactory;

    public $fillable = [
        'nom',
        'latitude',
        'longitude',
    ];

    public $timestamps = false;

    public function spots()
    {
        return $this->hasManyThrough('App\Models\Spot', 'App\Models\VillesSpot');
    }
}
