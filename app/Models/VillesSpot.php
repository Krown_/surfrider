<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VillesSpot extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function ville()
    {
        return $this->belongsTo('App\Models\Ville');
    }

    public function spots()
    {
        return $this->belongsTo('App\Models\Spot');
    }
}
