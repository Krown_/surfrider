<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProduitReport extends Model
{
    use HasFactory;

    public function produit() {
        return $this->hasOne('App\Models\Produit');
    }

    public function incident() {
        return $this->hasOne('App\Models\Incident');
    }
}
